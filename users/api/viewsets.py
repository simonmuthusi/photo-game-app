# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 20:06:29
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-27 20:06:47
# Project: photo-game

from django.contrib.auth import get_user_model

from rest_framework import viewsets

from .serializers import UserSerializer


User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):

    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer
