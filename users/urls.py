# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-29 19:35:13
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-29 19:40:36
# Project: photo-game
from django.conf.urls import include, url

from users.api.views import CustomAuthToken
from users.api.viewsets import UserViewSet

from rest_framework import routers
from rest_framework.authtoken import views

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
urlpatterns = [
    url(r'^api/v1/users/login/', CustomAuthToken.as_view()),
    url(r'^api/v1/', include(router.urls)),
]
