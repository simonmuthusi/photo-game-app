# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 20:42:59
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-27 20:52:09
# Project: photo-game


from rest_framework import viewsets

from photos.api.serializers import CategorySerializer, PhotosSerializer, VotingSerializer
from photos.models import Category, Photos, Voting


class CategoryViewSet(viewsets.ModelViewSet):

    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class PhotosViewSet(viewsets.ModelViewSet):

    queryset = Photos.objects.all()
    serializer_class = PhotosSerializer


class VotingViewSet(viewsets.ModelViewSet):

    queryset = Voting.objects.all()
    serializer_class = VotingSerializer
