# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 20:42:51
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-29 17:26:45
# Project: photo-game

from rest_framework import serializers

from photos.models import Category, Photos, Voting


class CategorySerializer(serializers.ModelSerializer):
    """
    Category serializer
    """

    class Meta:
        model = Category
        fields = (
            'id', 'name', 'is_active',
        )


class PhotosSerializer(serializers.ModelSerializer):
    """
    Photos serializer
    """

    class Meta:
        model = Photos
        fields = (
            'id', 'title', 'description', 'photo_file', 'category', 'owner', 'location', 'time_taken', 'is_active',
        )


class VotingSerializer(serializers.ModelSerializer):
    """
    Voting serializer
    """

    class Meta:
        model = Voting
        fields = (
            'id', 'photo', 'vote_type', 'vote_time', 'is_active'
        )
