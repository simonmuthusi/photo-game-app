# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 20:40:44
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-27 20:41:51
# Project: photo-game
VOTE_TYPES = (
    ("up", "up"),
    ("down", "down"),
    ("view", "view"),
)
