# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 20:48:31
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-27 20:54:34
# Project: photo-game
from django.conf.urls import include, url

from photos.api.viewsets import CategoryViewSet, PhotosViewSet, VotingViewSet

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet)
router.register(r'photos', PhotosViewSet)
router.register(r'voting', VotingViewSet)

urlpatterns = [
    url(r'^api/v1/', include(router.urls)),
]
