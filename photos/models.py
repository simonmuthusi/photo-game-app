# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 19:51:08
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-02-05 20:37:18
# Project: photo-game

from django.contrib.auth import get_user_model
from django.db import models

from model_utils.models import TimeStampedModel

from photos.const import VOTE_TYPES

User = get_user_model()


class Category(TimeStampedModel):
    '''Photo categories'''

    name = models.CharField(max_length=255, unique=True)
    is_active = models.BooleanField(default=True)


class Photos(TimeStampedModel):
    '''Store info for a given photo'''

    title = models.CharField(max_length=255)
    description = models.TextField(max_length=255)
    category = models.TextField(max_length=255)
    owner = models.ForeignKey(User)
    location = models.CharField(max_length=255, null=True, blank=True)
    time_taken = models.DateTimeField()
    photo_file = models.ImageField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('title', 'owner')


class Voting(TimeStampedModel):
    '''Voting options'''

    photo = models.ForeignKey(Photos)
    vote_type = models.CharField(
        max_length=255, choices=VOTE_TYPES, default='up')
    vote_time = models.DateTimeField()
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ('photo', 'vote_type', 'vote_time')
