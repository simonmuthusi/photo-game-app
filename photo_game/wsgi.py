# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 19:50:52
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-27 19:58:40
# Project: photo-game


import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "photo_game.settings")

application = get_wsgi_application()
