# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 19:50:52
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-02-04 20:38:44
# Project: photo-game
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from photos import urls as photos_urls
from users import urls as users_urls


urlpatterns = [
    url(r'^', include(users_urls, namespace="users")),
    url(r'^', include(photos_urls, namespace="photos")),
    url(r'^admin/', admin.site.urls),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
