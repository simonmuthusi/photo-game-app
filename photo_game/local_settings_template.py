# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-01-27 19:58:54
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-01-27 20:01:09
# Project: photo-game

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '<database name>',
        'USER': '<atabase user>',
        'PORT': '<postgres port>',
        'HOST': '<database host>',
        'PASSWORD': '<database password>'
    }
}


DEBUG = True

# add allowed hosts
ALLOWED_HOSTS = []


# set a security key
SECRET_KEY = ''

# add static URL
STATIC_URL = '/static/'
