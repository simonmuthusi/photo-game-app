## PhotoGame App Backend

This is a Django project using PostgreSQL as the database backend and running on Python 3. (Database migrations are tracked)

## Description
A simple mobile application allows users to upload pictures that other users can vote up
or vote down. The current winner is the one whose pictures have been both viewed the
most and up-voted the most, and least down-voted.

### Installation
Clone the repo:

`git clone https://bitbucket.org/simonmuthusi/photo-game-app`

Install the required packages in a new Python virtual environment:

`pip install -r requirement.txt`
If you're using virtualenvwrapper to manage your virtual environments you can create the env and install the packages in one go like so:

After installing the required apps, configure database settings under `local_settings.py`, then migrate `./manage.py migrate`, create a user who cna be used to login `./manage.py createsuperuser`

## Screenshots 

User Listing APIs
GET [root URL]/api/v1/users/
```
[
    {
        "id": 1,
        "username": "simonmuthusi",
        "email": "simonmuthusi@gmail.com",
        "first_name": "",
        "last_name": "",
        "groups": []
    }
]
```

Photo Listing APIs
GET [root URL]/api/v1/photos/
```[
    {
        "id": 1,
        "title": "A Testing Picture",
        "description": "A Testing Picture",
        "photo_file": "http://simon-photo-game-app.herokuapp.com/media/settings-gears.png",
        "category": 1,
        "owner": 1,
        "location": "Location",
        "time_taken": "2019-09-09T09:09:00Z",
        "is_active": true
    }
]
```

Photo Single View
GET [root URL]/api/v1/photos/1/
```
{
    "id": 1,
    "title": "A Testing Picture",
    "description": "A Testing Picture",
    "photo_file": "http://simon-photo-game-app.herokuapp.com/media/settings-gears.png",
    "category": 1,
    "owner": 1,
    "location": "Location",
    "time_taken": "2019-09-09T09:09:00Z",
    "is_active": true
}
```


